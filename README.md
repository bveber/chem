chem is a package intended to provide a structured methodology for QSAR regression modeling.

To install:
* Clone this repository with a git tool (command line or GUI)
* Install python3
* Navigate to the top level directory of the cloned repository
* run command: python3 setup.py install
* to test, run command: python3 test.py
