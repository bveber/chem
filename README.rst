chem is a package intended to provide a structured methodology for QSAR regression modelling.

To install:
Clone this repository with a git tool (command line or GUI)
Install python3
Navigate to the top level directory of the cloned repository and use ``setup.py``::

    python3 setup.py install

To run the test with the interpreter available as ``python``, use::

    python3 test.py

